export class Picture {
    id: number;
    userId: number;
    datetime : Date;
    picture :  string | ArrayBuffer;
    comments : Comment[];

  constructor (
    id: number,
    userId: number,
    datetime : Date,
    picture :  string | ArrayBuffer,
    comments : Comment[]

  ) {
    this.id = id;
    this.userId = userId;
    this.datetime = datetime;
    this.picture = picture;
    this.comments = comments;
  }

  addComment(comment: Comment) {
    this.comments.push(comment);
  }

  setComments(com: Comment[]) {
  console.log(com);
    this.comments = com;
  }

}
