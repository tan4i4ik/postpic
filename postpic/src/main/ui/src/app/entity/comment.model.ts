export class Comment {
  constructor (
    public id: number,
    public userId: number,
    public pictureId : number,
    public datetime : Date,
    public text: string
  ) {}

}
