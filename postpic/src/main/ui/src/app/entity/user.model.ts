import { Picture } from './picture.model';
export class User {
    id: number;
    login : string;
    avatar : string | ArrayBuffer;
    password : string;
    name : string;
    role : string;
    email : string;
    pictures : Picture[];

  constructor (id: number, login : string, avatar: string  | ArrayBuffer, password : string, name : string, role : string, email : string, pictures : Picture[]) {
    this.id = id;
    this.login = login;
    this.avatar = avatar;
    this.password = password;
    this.name = name;
    this.role = role;
    this.email = email;
    this.pictures = pictures;

  }
}
