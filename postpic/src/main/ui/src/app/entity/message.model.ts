export class Message {
  constructor (
    public id: number,
    public userIdSource: number,
    public userIdDest : number,
    public datetime : Date,
    public text: string
  ) {}

}
