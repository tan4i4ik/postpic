import { Component, OnInit} from '@angular/core';
import { User } from './../entity/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './../service/user.service';
import { PictureService } from './../service/picture.service';
import { CommentService } from './../service/comment.service';
import { Comment } from './../entity/comment.model';
import { Picture } from './../entity/picture.model';

@Component({
  selector: 'app-profilepage',
  templateUrl: './profilepage.component.html',
  styleUrls: ['./profilepage.component.css']
})
export class ProfilepageComponent implements OnInit {

  private userId;
  private user: User = new User(-1, "" , "", "","", "", "", new Array());
  private inputData = new Array();
  access: number = 1;
  constructor(private route: ActivatedRoute,
              private userService : UserService,
              private pictureService : PictureService,
              private commentService : CommentService,
              private router: Router) { }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.userId = +params['userid'] || 0;
      });
    this.userService.getFullUser(this.userId)
      .subscribe(usr => {
        var pictures = new Array();
        usr["pictures"].forEach(picture => {
          var comments = new Array();
          picture["comments"].forEach(comment => {
            comments.push(new Comment(comment["id"], comment["userId"], comment["pictureId"], comment["datetime"], comment["text"]));
          });
          pictures.push(new Picture(picture["id"], picture["userId"], picture["datetime"], 'data:image/png;base64,' +  picture["picture"] , comments));
          this.inputData.push('');
        });
        this.user = new User(usr["id"], usr["login"], 'data:image/png;base64,' + usr["avatar"], "",usr["name"], "", usr["email"], pictures);
        console.log(this.user);
      },
      err => {
        console.log(err);
      });
  }


}
