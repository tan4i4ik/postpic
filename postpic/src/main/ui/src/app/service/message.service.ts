import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from './../entity/message.model';


@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) { }

  public addMsg(userIdSource: number, userIdDest: number, txt: string){
   return this.http.post('/userSource='+ userIdSource + '&userDest=' + userIdDest + '/addmessage', txt);
  }

  public dltMsg(userId: number, msgId: number){
   return this.http.delete('/userId=' + userId + '&messageId='+ msgId +'/delete');
  }

  public getMessages(idSourceUser: number, idDestUser: number) {
    return this.http.get<Message[]>('/userSource=' + idSourceUser + '&userDest=' + idDestUser + '/get');
  }
}
