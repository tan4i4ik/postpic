import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Picture } from './../entity/picture.model';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  public addComment(userId: number, pictureId: number, text: string){
   return this.http.post('/user='+ userId + '&picture=' + pictureId + '/addcomment', text);
  }

  public deleteComment(userId: number, pictureId: number, commentId: number){
   return this.http.delete('/user='+ userId + '&picture=' + pictureId + '&comment=' + commentId + '/delete');
  }
}

