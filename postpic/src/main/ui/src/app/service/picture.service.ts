import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Picture } from './../entity/picture.model';

@Injectable({
  providedIn: 'root'
})
export class PictureService {

  constructor(private http: HttpClient) { }

  public getPictures() {
    return this.http.get<Picture[]>('/pictures');
  }


  public addPicture(userId: number, picture: Picture) {
      console.log(picture);
     return this.http.post('/user=' + userId+ '/addpicture', picture);
  }

  public getPicture(pictureId: number) {
    return this.http.get('/picture=' + pictureId);
  }

  public deletePicture(userId: number,pictureId: number) {
    return this.http.delete('/userid=' +userId +'&pic='+pictureId+'/delete');
  }
}
