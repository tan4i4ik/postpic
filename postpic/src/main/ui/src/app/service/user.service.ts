import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './../entity/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users: User[] = new Array();

  constructor( private http: HttpClient) {
    this.http.get<User[]>('/usersdata').subscribe(users => users.forEach(
    user =>  {
      this.users.push(new User(user["id"], user["login"],'data:image/png;base64,' +  user["avatar"],"","","","", new Array()));
    }));
  }

  public getUsers() {
    return this.users;
  }
  public addUser (data: User) {
      return this.http.post('/adduser', data);
  }

  public getUser(id: number) {
   return this.http.post('/userById', id);
  }


  public getShortUser(id: number) {
   return this.http.get('/usershortdata/userid=' + id);
  }

  public getFullUser(id: number) {
   return this.http.get('/getfulluser/userid=' + id);
  }

  public editUser(id: number, user:User) {
   return this.http.post('/edit/user='+id, user);
  }


  public login(login:string, password: string) {
    return this.http.post('/login', {'login' : login, 'password' : password});
  }

  public changeUser(id: number,  avatar : string | ArrayBuffer) {
    this.users.forEach(user => {
      if(user.id === id) {
        user.avatar = avatar;
        user.name = name;
      }
    });
  }

  public delete(id: number) {
    return this.http.post('/deleteuser', id);
  }

  public getUserById(id: number){
    for (let user of this.users) {
      if(user.id === id) {
        return user;
      }
    }
  }

  public deleteUserFromList(userId: number) {
    var index = this.users.findIndex(x => x.id === userId);
    this.users.splice(index, 1);
  }

    public addUserToList(user: User) {
      this.users.push(user);
    }
}
