import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoggedMainpageComponent } from './logged-mainpage/logged-mainpage.component';
import { EdituserComponent } from './edituser/edituser.component';
import { ProfilepageComponent } from './profilepage/profilepage.component';
import { PicturepageComponent } from './picturepage/picturepage.component';
import { ChatpageComponent } from './chatpage/chatpage.component';
import { PicturesListComponent } from './pictures-list/pictures-list.component';
import { FilterPipe } from './chatpage/filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainpageComponent,
    RegistrationComponent,
    LoggedMainpageComponent,
    EdituserComponent,
    ProfilepageComponent,
    PicturepageComponent,
    ChatpageComponent,
    PicturesListComponent,
    FilterPipe,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
