import { Component, OnInit } from '@angular/core';
import { User } from './../entity/user.model';
import { UserService } from './../service/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {


  private user: User = new User(-1,"","","","","","", new Array());
  private userId: number;
  private chgPass: boolean = true;
  private incorrectEdit: boolean = false;
  private filestring;
  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private router: Router,) { }

  ngOnInit() {

    this.route
      .queryParams
      .subscribe(params => {
        this.userId = +params['userid'] || 0;
    });

    /****load user data*******/
    this.userService.getUser(this.userId)
      .subscribe(result => {
        this.user = new User(result["id"], result["login"], 'data:image/png;base64,' + result["avatar"], "",result["name"], "", result["email"], new Array());
        this.filestring = 'data:image/png;base64,' + result["avatar"];
    });
  }

  /********show/hide change password tags *********/
  private changePassword() {
    this.chgPass = !this.chgPass;
  }

  /**********load picture on the page***********/
   private getFile(event:any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
          this.filestring = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
   }

  /*****change user's data*********/
   private save() {
      let k = this.userService.editUser(this.userId, new User(this.user.id, this.user.login, new String(this.filestring).split(',')[1], this.user.password, this.user.name, "", this.user.email, new Array()))
      .subscribe(result => {
        if(result === true) {
          this.userService.changeUser(this.user.id, this.filestring);
          this.router.navigate(['..','userlogged'], { queryParams: { userid: this.userId } });
        } else {
         this.incorrectEdit = true;
        }
      });
   }

  private getBack() {
    this.router.navigate(['..','userlogged'], { queryParams: { userid: this.userId } });
  }



}
