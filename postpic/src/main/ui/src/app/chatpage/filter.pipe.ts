import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
/****filter data******/
export class FilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(!args)
      return value;
    return value.filter(
      item => item.login.toLowerCase().indexOf(args.toLowerCase()) > -1
    );
  }

}
