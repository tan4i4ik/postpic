import { Component, OnInit } from '@angular/core';
import { UserService } from './../service/user.service';
import { User } from './../entity/user.model';
import { MessageService } from './../service/message.service';
import { Message } from './../entity/message.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-chatpage',
  templateUrl: './chatpage.component.html',
  styleUrls: ['./chatpage.component.css']
})
export class ChatpageComponent implements OnInit {

  /*****list of users*********/
  private users : User[];


  private userId: number = -1;

  /*******get data about user in chat***********/
  private currDestUserImg: string | ArrayBuffer;
  private currDestUserLogin: string;
  private currDestUserId: number;

  private messages: Message[] = new Array();

  private txt ="";

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private messageService: MessageService) { }

  /********get chat *******************/
  private getChat(id: number, avatar: string | ArrayBuffer, login: string) {
    this.currDestUserImg = avatar;
    this.currDestUserLogin = login;
    this.currDestUserId = id;
    this.messageService.getMessages(this.userId, id).subscribe(
    messages => {
      var msgs: Message[] = new Array();
      messages.forEach(msg => {
        msgs.push(new Message(msg["id"], msg["userSourceId"], msg["userDestId"], msg["datetime"], msg["text"]));

      })
      this.messages = msgs;

      }, err => {
        console.log(err);
      }
    );

  }

  private addMessage() {
     this.messageService.addMsg(this.userId, this.currDestUserId, this.txt).subscribe(
      success => {
        this.messages.push(new Message(success["id"],this.userId,this.currDestUserId, success["datetime"], success["text"]));
        console.log();
        this.txt="";
      },
      err => {
        console.log(err);
      }
     );
  }

  private  deleteMessage(id: number, index: number) {
    this.messageService.dltMsg(this.userId, id).subscribe(
        success => {
            if(success === true) {
               this.messages.splice(index, 1);
            }
        },
        err => {
          console.log(err);
        }
       );
    }

  private getUserById(id: number) {
    return this.userService.getUserById(id);
  }

  /******show users by search data************/
  private searchUser(event) {
    this.users.filter(m => m.login);
    console.log(event);

  }

  ngOnInit() {
    var userId: number;
    this.route
      .queryParams
      .subscribe(params => {
        this.userId = +params['userid'] || 0;
       });
    this.users = this.userService.getUsers();
    this.getChat(this.users[0].id,this.users[0].avatar, this.users[0].login);
  }

}
