import { Component, OnInit, Input } from '@angular/core';
import { Picture } from './../entity/picture.model';
import { Comment } from './../entity/comment.model';
import { CommentService } from './../service/comment.service';
import { PictureService } from './../service/picture.service';
import { UserService } from './../service/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pictures-list',
  templateUrl: './pictures-list.component.html',
  styleUrls: ['./pictures-list.component.css']
})
export class PicturesListComponent implements OnInit {

  @Input('pictures')
  pictures: Picture[];

  /***-1-if user logged out; 0 - if logged in mainpage logged in user; 1 - if user profile page *************************************/
  @Input('access')
  access: number;

  private inputData = new Array();
  private userId: number;

  constructor(private commentService: CommentService,
              private userService: UserService,
              private pictureService: PictureService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.pictures.forEach(x =>this.inputData.push(''));
    /****get userId*****/
    this.route
      .queryParams
      .subscribe(params => {
        this.userId = +params['userid'] || 0;
    });
  }

  /*******get user data by id number************/
  public getUserById(id: number) {
     return this.userService.getUserById(id);
  }

  /******delete comment to picture by pictureId and commentId***************************/
  public deleteComment(id: number, pictureId: number, indexPic: number, indexCom: number) {
     this.commentService.deleteComment(this.userId, pictureId, id).subscribe(
      success => {
        if(success === true) {
          this.pictures[indexPic].comments.splice(indexCom, 1);
        }
      },
      err => {
        console.log(err);
      });
   }

  /*********delete picture by id******************/
  private deletePicture(id: number, i: number) {
    this.pictureService.deletePicture(this.userId, id)
     .subscribe(success => {
        if(success === true) {
          this.pictures.splice(i, 1);
        }
      },
      err => {
       console.log(err);
      });
  }

  /********add comment to picture where picture id is pictureId************/
  private saveComment(pictureId: number, j: number) {
    this.commentService.addComment(this.userId,pictureId, this.inputData[j]).subscribe(
      success => {
        var comments = new Array();
        this.pictures[j].comments.map(x => comments.push(x));
        comments.push(new Comment(success["id"], success["userId"], success["picture"], success["datetime"], success["text"]));
        this.pictures[j] = new Picture(this.pictures[j].id, this.pictures[j].userId, this.pictures[j].datetime, this.pictures[j].picture, comments);
        this.inputData[j] = '';
      },
      err => {
        console.log(err);
      });
  }

  /*******check if current input for comment is empty or not***************/
  private inputDataState(j: number) {
    return this.inputData[j] === '' ? true : false;
  }

  /*********get all information about picture******************/
  private showPicture (pictureId: number) {
    this.router.navigate(['..','picture'], { queryParams: { pictureid: pictureId, userid: this.userId} });
  }
}
