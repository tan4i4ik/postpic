import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges,SimpleChange } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from './../entity/user.model';
import { UserService } from './../service/user.service';
import { Picture } from './../entity/picture.model';
import { PictureService } from './../service/picture.service';
import { Comment } from './../entity/comment.model';
import { CommentService } from './../service/comment.service';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-logged-mainpage',
  templateUrl: './logged-mainpage.component.html',
  styleUrls: ['./logged-mainpage.component.css']
})
export class LoggedMainpageComponent implements OnInit{

  private userId: number = -1;
  private user: User = new User(-1,"","","","","","", new Array());
  private flag: boolean = false;
  private result;
  private closeResult: string;

  private filestring : string | ArrayBuffer = '';

  pictures: Picture[]  = new Array()  ;
  access: number = 0;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userService : UserService,
              private pictureService: PictureService,
              private modalService: NgbModal,
              private commentService: CommentService) {
   }


  private getUser (id: number) {
    let k = this.userService.getUser(id)
      .subscribe(result => {
        this.user = new User(result["id"], result["login"], 'data:image/png;base64,' + result["avatar"], "",result["name"], "", result["email"], new Array());
       });

  }

    public getPictures() {
      return this.pictureService.getPictures().subscribe(pictures => pictures.forEach(picture => {
      let comments = new Array();
        picture["comments"].forEach(comment => {
          comments.push(new Comment(comment["id"], comment["userId"], comment["pictureId"], comment["datetime"], comment["text"]));
        });
        this.pictures.push(new Picture(picture["id"], picture["userId"], picture["datetime"], 'data:image/png;base64,' +  picture["picture"] , comments));
      }));
    }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.userId = +params['userid'] || 0;
    });
    this.getUser (this.userId );
    this.getPictures();
  }


  private openUserProfile(){
      this.router.navigate(['..','userlogged','profilepage'], { queryParams: { userid: this.userId } });
  }

  /****open modal to add picture***********/
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  /*****load picture on the page**********/
  getFile(event:any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: ProgressEvent) => {
        this.filestring = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  /******save picture**********/
  private savePic() {
    if(new String(this.filestring).split(',')[1] === '') {
      return;
    }
    var res = this.pictureService.addPicture(this.userId, new Picture(-1, this.userId, null, new String(this.filestring).split(',')[1], null)).subscribe(picture => {
      if(picture !== null) {
        this.pictures.splice(0, 0, new Picture(picture["id"], picture["userId"], picture["datetime"], this.filestring , new Array()));
      }
    },
    err => {
      console.log(err);
    });
  }




  private openChat() {
       this.router.navigate(['..','userlogged', 'chat'], { queryParams: {userid: this.userId} });

  }
}
