import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedMainpageComponent } from './logged-mainpage.component';

describe('LoggedMainpageComponent', () => {
  let component: LoggedMainpageComponent;
  let fixture: ComponentFixture<LoggedMainpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoggedMainpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedMainpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
