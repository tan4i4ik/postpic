import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent} from './login/login.component';
import { MainpageComponent} from './mainpage/mainpage.component';
import { RegistrationComponent} from './registration/registration.component';
import { LoggedMainpageComponent} from './logged-mainpage/logged-mainpage.component';
import { EdituserComponent} from './edituser/edituser.component';
import { ProfilepageComponent} from './profilepage/profilepage.component';
import { PicturepageComponent} from './picturepage/picturepage.component';
import { ChatpageComponent} from './chatpage/chatpage.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: '', component: MainpageComponent },
  { path: 'userlogged',
    children: [
      {
        path: '',
        component: LoggedMainpageComponent,
      },
      {
        path:'edit',
        component: EdituserComponent,
      },
      {
        path: 'profilepage',
        component: ProfilepageComponent,
      },
      {
        path: 'chat',
        component: ChatpageComponent,
      }
    ]
  },

  {
    path: 'picture', component: PicturepageComponent
  }



];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
