import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicturepageComponent } from './picturepage.component';

describe('PicturepageComponent', () => {
  let component: PicturepageComponent;
  let fixture: ComponentFixture<PicturepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PicturepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicturepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
