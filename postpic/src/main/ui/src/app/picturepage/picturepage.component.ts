import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Picture } from './../entity/picture.model';
import { User } from './../entity/user.model';
import { Comment } from './../entity/comment.model';
import { UserService } from './../service/user.service';
import { PictureService } from './../service/picture.service';
import { CommentService } from './../service/comment.service';

@Component({
  selector: 'app-picturepage',
  templateUrl: './picturepage.component.html',
  styleUrls: ['./picturepage.component.css']
})
export class PicturepageComponent implements OnInit {

  private pictureId;
  private picture : Picture = new Picture(-1,-1, new Date(),"",new Array());
  private userId;
  private comments: Comment[] = new Array();
  private comment: string;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private pictureService : PictureService,
              private userService: UserService,
              private commentService: CommentService) { }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.pictureId = +params['pictureid'] || 0;
        this.userId = +params['userid'] || 0;
    });
    this.pictureService.getPicture(this.pictureId).subscribe(
      success => {
        success["comments"].forEach(
          comment => {
            this.comments.push(new Comment(comment["id"], comment["userId"], comment["pictureId"], comment["datetime"], comment["text"]));
        });
        this.picture = new Picture(success["id"], success["userId"], success["datetime"], 'data:image/png;base64,' +  success["picture"] , new Array());
      },
      err => {
        console.log(err);
      }
    );
   }


  public getUserById(id: number) {
     return this.userService.getUserById(id);
  }

  public saveComment() {
    this.commentService.addComment(this.userId,this.pictureId, this.comment)
      .subscribe(success => {
        this.comments.push(new Comment(success["id"], success["userId"], success["picture"], success["datetime"], success["text"]));
        this.comment='';
      },
      err => {
        console.log(err);
    });
  }

  public deleteComment(id: number, index: number) {
     this.commentService.deleteComment(this.userId,this.pictureId, id)
      .subscribe(success => {
        if(success === true) {
          this.comments.splice(index, 1);
        }
      },
      err => {
        console.log(err);
      });
  }
}
