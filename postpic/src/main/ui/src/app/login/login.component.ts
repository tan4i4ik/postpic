import { Component, OnInit } from '@angular/core';
import { UserService } from './../service/user.service';
import { Router } from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private result: boolean = false;
  private user = {};

  constructor(private userService: UserService, private router: Router) { }

  logging (login: string, password: string) {
    console.log(this.user);
    this.userService.login(login,password).subscribe(
      success => {
        if(success !== -1) {
           this.router.navigate(['/userlogged'], { queryParams: { userid: success } });
        } else {
          this.result = true;
          login='';
          password='';
        }
      },
      err => {
        console.log(err);
      });

  }

  ngOnInit() {
  }

}
