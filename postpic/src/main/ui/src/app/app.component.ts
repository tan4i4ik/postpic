import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './entity/user.model';
import { UserService } from './service/user.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private userId: number = -1;

  private filestring : string | ArrayBuffer;

  constructor(private route: ActivatedRoute,private router: Router, private userService: UserService) {}

  /******delete user account********/
  private deleteUser() {
    this.route
      .queryParams
      .subscribe(params => {
        this.userId = +params['userid'] || 0;
      });
    this.userService.delete(this.userId).subscribe(result => {
      if(result === true) {
        this.userService.deleteUserFromList(this.userId );
        this.router.navigate(['/']);
      }
    });
  }

  /******navigate to edit user page********/
  private editBtn() {
    this.route
      .queryParams
      .subscribe(params => {
        this.userId = +params['userid'] || 0;
      });
    this.router.navigate(['..','userlogged','edit'], { queryParams: { userid: this.userId } });
  }

  /*******navigate to logged user main page*************/
  private btnToMainLoggedInPage() {
    this.route
      .queryParams
      .subscribe(params => {
        this.userId = +params['userid'] || 0;
      });
    this.router.navigate(['..','userlogged'], { queryParams: { userid: this.userId } });
  }

  /*****get user avatar on header*************/
  private getImg() {
    this.route
      .queryParams
      .subscribe(params => {
        this.userId = +params['userid'] || 0;
      });
    return this.userService.getUserById(this.userId).avatar;
  }

  ngOnInit() {
  }
}
