import { Component, OnInit } from '@angular/core';
import { Picture } from './../entity/picture.model';
import { User } from './../entity/user.model';
import { Comment } from './../entity/comment.model';
import { UserService } from './../service/user.service';
import { PictureService } from './../service/picture.service';
import { ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {
  pictures: Picture[]  = new Array()  ;
  users: User[] = new Array() ;
  private inputDisabled: boolean = false;
  access: number = -1;

  constructor(private userService: UserService,
              private pictureService: PictureService,
              private route: ActivatedRoute,
              private router: Router) {}

  public getPictures() {
    return this.pictureService.getPictures().subscribe(pictures => pictures.forEach(picture => {
      let comments = new Array();
      picture["comments"].forEach(comment => {
        comments.push(new Comment(comment["id"], comment["userId"], comment["pictureId"], comment["datetime"], comment["text"]));
      });
      this.pictures.push(new Picture(picture["id"], picture["userId"], picture["datetime"], 'data:image/png;base64,' +  picture["picture"] , comments));
    }));
  }

  ngOnInit(): void {
    this.getPictures();
  }



}
