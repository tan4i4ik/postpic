package org.tan4i4ik;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.tan4i4ik.configuration.ConfigDB;
import org.tan4i4ik.consoleapp.ConsoleApp;


@ComponentScan(basePackages = "org.tan4i4ik")
@Import(ConfigDB.class)
public class Main {


    public static void main(String[] args) {

        ApplicationContext context
                = new AnnotationConfigApplicationContext(Main.class);

        ConsoleApp p = context.getBean(ConsoleApp.class);
        p.mainConsole();
    }

}

