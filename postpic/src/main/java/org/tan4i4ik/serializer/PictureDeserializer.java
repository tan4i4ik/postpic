package org.tan4i4ik.serializer;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.tan4i4ik.model.Picture;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PictureDeserializer extends JsonDeserializer<Picture> {
    @Override
    public Picture deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        Long userId = node.has("userId") ? node.get("userId").asLong() : -1L;

        byte[] picture = node.has("picture") ? node.get("picture").binaryValue() : null;

        return new Picture(userId,picture);
    }
}
