package org.tan4i4ik.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.tan4i4ik.model.Picture;


import java.io.IOException;

public class PictureSerializer extends JsonSerializer<Picture> {

    @Override
    public void serialize(Picture picture, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("id");
        jsonGenerator.writeNumber(picture.getId());
        jsonGenerator.writeFieldName("userId");
        jsonGenerator.writeNumber(picture.getUserId());
        jsonGenerator.writeFieldName("picture");
        jsonGenerator.writeString(Base64.encode(picture.getPicture()));
        jsonGenerator.writeFieldName("datetime");
        jsonGenerator.writeString(picture.getDatetime().toString());
        jsonGenerator.writeFieldName("comments");
        jsonGenerator.writeObject(picture.getCommentList());
        jsonGenerator.writeEndObject();
    }
}