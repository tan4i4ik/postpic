package org.tan4i4ik.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.tan4i4ik.model.User;

import java.io.IOException;

public class UserDeserializer extends JsonDeserializer<User> {
    @Override
    public User deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String login = node.get("login").asText();
        String password = node.get("password").asText();
        String name = node.has("name") ? node.get("name").asText() : "";
        String role = node.has("role")? node.get("role").asText() : "";
        String email = node.has("email") ? node.get("email").asText() : "";

        byte[] avatar = node.has("avatar") ? node.get("avatar").binaryValue() : null;

        return new User(login,avatar,password,name,role,email);
    }
}
