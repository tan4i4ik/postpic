package org.tan4i4ik.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.tan4i4ik.model.User;

import java.io.IOException;

public class UserSerializer extends JsonSerializer<User> {

@Override
public void serialize(User user, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException, JsonProcessingException {

        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("id");
        jsonGenerator.writeNumber(user.getId());
        jsonGenerator.writeFieldName("login");
        jsonGenerator.writeString(user.getLogin());
        jsonGenerator.writeFieldName("avatar");
        jsonGenerator.writeString(Base64.encode(user.getAvatar()));
        jsonGenerator.writeFieldName("password");
        jsonGenerator.writeString(user.getPassword());
        jsonGenerator.writeFieldName("name");
        jsonGenerator.writeString(user.getName());
        jsonGenerator.writeFieldName("email");
        jsonGenerator.writeString(user.getEmail());
        jsonGenerator.writeFieldName("pictures");
        jsonGenerator.writeObject(user.getPictureList());
        jsonGenerator.writeEndObject();
        }
}