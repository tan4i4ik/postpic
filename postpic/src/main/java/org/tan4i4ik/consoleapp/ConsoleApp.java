package org.tan4i4ik.consoleapp;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tan4i4ik.model.Comment;
import org.tan4i4ik.model.Message;
import org.tan4i4ik.model.Picture;
import org.tan4i4ik.model.User;
import org.tan4i4ik.service.CommentService;
import org.tan4i4ik.service.MessageService;
import org.tan4i4ik.service.PictureService;
import org.tan4i4ik.service.UserService;

import java.io.*;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

@Component
public class ConsoleApp {

    private static Scanner sc = new Scanner(System.in);

    @Autowired
    private UserService us;

    @Autowired
    private PictureService ps;

    @Autowired
    private CommentService cs;

    @Autowired
    private MessageService ms;


    /*************get access to user data**************************/
    private void login() {
        System.out.println("Input login");
        String login = sc.next();

        System.out.println("Input password");
        String password = sc.next();

        boolean res = us.login(login,password);
        if (res) {
            authorizedAccess(us.getUser(login));
        }
    }

    /*****data that is accessed for authorization*********/
    private void authorizedAccess(User user) {
        /**********get user's pictures*******************/
        int i;
        do {

            System.out.println("Hello " + user.getLogin());
            System.out.println("1. Edit profile");
            System.out.println("2. Add picture");
            System.out.println("3. Delete picture");
            System.out.println("4. Show account information");
            System.out.println("5. Show all pictures ");
            System.out.println("6. Get comments to picture");
            System.out.println("7. Leave comment to picture");
            System.out.println("8. Delete comment to picture");
            System.out.println("9. Delete user");
            System.out.println("10. Log out");

            System.out.println("Choose your next step(1-9)");

            try {
                i = Integer.parseInt(sc.next());
            } catch (NumberFormatException e) {
                i = -1;
            }
            switch (i) {
                case 1:
                    editAccount(user);
                    break;
                case 2:
                    addPictureUser(user.getId());
                    break;
                case 3:
                    deletePicture(user);
                    break;
                case 4:
                    showAccountInfo(user);
                    break;
                case 5:
                    showPictures();
                    break;
                case 6:
                    getComment();
                    break;
                case 7:
                    leaveComment(user.getId());
                    break;
                case 8:
                    deleteComment(user.getLogin());
                    break;
                case 9:
                    us.deleteUser(user.getLogin());
                    break;
                default:break;
            }

            System.out.println("------------------------------------------------");
        } while (i != 10);
    }

    private void deleteComment(String login) {
        List<Comment> list = getComment();
        if(list == null) {
            return;
        }
        System.out.println("Choose comment for deleting");
        int i =-1;
        do {
            try {
                i = Integer.parseInt(sc.next());
            } catch (NumberFormatException e) {
                System.out.println("Try again");
                i = -1;
            }

        }while (i < 1 || i > list.size() + 1);


     //   cs.deleteComment(list.get(i - 1), login);
    }

    private List<Comment> getComment() {
        List<Picture> list = showPictures();
        System.out.println("Choose number of picture");
        int i =-1;
        do {
            try {
                i = Integer.parseInt(sc.next());
            } catch (NumberFormatException e) {
                System.out.println("Try again");
                i = -1;
            }

        }while (i < 1 || i > list.size() + 1);

        List<Comment> listComment = cs.getComments(list.get(i - 1).getPictureId());
        if(listComment == null) {
            System.out.println("Picture doesn't have comments");
            return null;
        }
        int j = 1;
        for(Comment comment: listComment) {
            System.out.println(j++ +".Time: " + comment.getDate() + " Username: " + comment.getUserId() + " Text: " + comment.getText());
        }
        return listComment;
    }


    /***************delete picture by user******************************/
    private void deletePicture(User user) {
        showPicturesUser(user);
        System.out.println("Choose picture for deleting ");
        int i = Integer.parseInt(sc.next());


    }
    /**************show user information with pictures**************************/
    private void showAccountInfo(User user) {
        System.out.println("Login: " + user.getLogin());
        System.out.println("Name: " + user.getName());
        System.out.println("Email: " + user.getEmail());

    }

    private void showPicturesUser(User user) {
        int i = 1;
        for (Picture picture: ps.getListPicByUser(user.getLogin())) {
            System.out.println(i + ". Picture id: " +picture.getPictureId() + " Time of creating: " + picture.getDatetime() + "  userLogin: " + user.getLogin());
            i++;
        }

    }

    /***********add picture to user***********************************/
    private void addPictureUser(Long userId) {
        System.out.println("Add path to picture: ");
        String path = sc.next();
        File f = new File(path);
        if(f.exists()) {
            byte[] bytesArray = new byte[(int) f.length()];

            FileInputStream fis = null;
            try {
                fis = new FileInputStream(f);
                fis.read(bytesArray);
                fis.close();
                ps.addPicture(userId,bytesArray);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }

    private void editAccount(User user) {
        Pattern emailPattern = Pattern.compile("^(.+)@(.+)$");
        Pattern passwordPattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$");
        System.out.println("Do you want to change name? y/n");
        String nameChange = sc.next();
        if (nameChange.equals("y")) {
            System.out.println("Input new name");
            String name = sc.next();
            user.setName(name);
        }

        System.out.println("Do you want to change email? y/n");
        String emailChange = sc.next();

        if (emailChange.equals("y")) {
            String email;
            Boolean emailFlag;
            System.out.println("Input new email");
            do {
                email = sc.next();
                emailFlag = emailPattern.matcher(email).find();
                if(emailFlag == false) {
                    System.out.println("The email isn't correct. Try again");
                }
            } while (emailFlag == false);
            user.setEmail(email);
        }

        System.out.println("Do you want to change password? y/n");
        String passwordChange = sc.next();
        if(passwordChange.equals("y")) {
            System.out.println("Input password(must contains at least 6 letter with one uppercase, one lowercase and one number)");
            boolean passFlag;
            String password;
            do {
                password = sc.next();
                passFlag = passwordPattern.matcher(password).find();
                if (passFlag == false) {
                    System.out.println("The password isn't correct. Try again");
                }
            } while (passFlag == false);
            user.setPassword(password);
        }

        String ans;
        System.out.println("Edit account? y/n");
        ans = sc.next();

        if (ans.equals("y")) {
            us.editUser(user.getLogin(), user);
        }

        authorizedAccess(user);
    }

    private void createAccount() {
        Pattern emailPattern = Pattern.compile("^(.+)@(.+)$");
        Pattern passwordPattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$");
        System.out.println("Input login");
        String login = sc.next();

        System.out.println("Input name");
        String name = sc.next();

        System.out.println("Input email");
        boolean emailFlag;
        String email;
        do {
            email = sc.next();
            emailFlag = emailPattern.matcher(email).find();
            if(emailFlag == false) {
                System.out.println("The email isn't correct. Try again");
            }
        } while (emailFlag == false);


        System.out.println("Input password(must contains at least 6 letter with one uppercase, one lowercase and one number)");
        boolean passFlag;
        String password;
        do {
            password = sc.next();
            passFlag = passwordPattern.matcher(password).find();
            if(passFlag == false) {
                System.out.println("The password isn't correct. Try again");
            }
        } while (passFlag == false);

        String ans;

        System.out.println("Create account? y/n");
        ans = sc.next();

        if (ans.equals("y")) {
            if(us.createUser(login,name,null,email, password) != -1L) {
                System.out.println("Account was created");
            }   else {
                System.out.println("Account wasn't created");
            }
        }
    }

    private List<Picture> showPictures() {
        int i = 1;
        List<Picture> list = ps.getListPics();
        for(Picture picture: list) {
            System.out.println(i + ". Picture id: " +picture.getPictureId() + " Time of creating: " + picture.getDatetime() + "  userLogin: " + picture.getUserId());
            i++;
        }

        return list;
    }

    private void leaveComment(Long userId) {
        List<Picture> list = showPictures();
        System.out.println("Do you want to leave comment under picture?y/n");
        String in = sc.next();
        if(in.equals("y")) {
            System.out.println("Choose number of picture");
            Integer num;
            do {
                try {
                    num = Integer.parseInt(sc.next());
                } catch (Exception e) {
                    num = -1;
                }
            } while (num > list.size() + 1 || num < 0);
            System.out.println("Input your comment");
            sc.nextLine();
            String comment = sc.nextLine();
            if(num > 0 && num < list.size() + 1) {
                cs.addComment(userId, list.get(num - 1).getPictureId(), comment);
            }
        }

    }




    public void mainConsole() {
        int i;
        do {
            System.out.println("Welcome to application");

            System.out.println("1. If you don't have an account yet");
            System.out.println("2. If you created an account before");
            System.out.println("3. Show pictures");
            System.out.println("4. Stop the application");

            do {
                System.out.println("Choose your next step(1-4)");
                try {
                    i = Integer.parseInt(sc.next());
                } catch (Exception e) {
                    System.out.println("Try again");
                    i = 7;
                }
            } while (i > 4 || i < 0);

            switch (i) {
                case 1:
                    createAccount();
                    break;
                case 2:
                    login();
                    break;
                case 3:
                    showPictures();
                    break;
                case 4:
                    System.exit(0);
                    break;

            }

            System.out.println("\n=======================================================\n");
        } while (i != 4 );

    }

}
