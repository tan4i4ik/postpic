package org.tan4i4ik.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.tan4i4ik.serializer.PictureDeserializer;
import org.tan4i4ik.serializer.PictureSerializer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonSerialize(using = PictureSerializer.class)
@JsonDeserialize(using = PictureDeserializer.class)
public class Picture implements Serializable {

    private static final long serialVersionUID = 6529685098267757690L;

    private Long id;
    private Long userId;
    private byte[] picture;
    private Date datetime;
    private List<Comment> commentList;


    public Picture(Long pictureId, Long userId, byte[] picPath, Date datetime) {
        this.id = pictureId;
        this.userId = userId;
        this.picture = picPath;
        this.datetime = datetime;
        this.commentList = new ArrayList<>();
    }

    public Picture(Long userId, byte[] picPath, Date datetime) {
        this.userId = userId;
        this.picture = picPath;
        this.datetime = datetime;
        this.commentList = new ArrayList<>();
    }

    public Picture(long id, long userId, byte[] picture) {
        this.id = id;
        this.userId = userId;
        this.picture = picture;
        this.commentList = new ArrayList<>();
    }

    public Picture(Long id, Long userId, Date datetime) {
        this.id = id;
        this.userId = userId;
        this.datetime = datetime;
        this.commentList = new ArrayList<>();
    }

    public Picture(Long userId, byte[] picture) {
        this.userId = userId;
        this.picture = picture;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picPath) {
        this.picture = picPath;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Long getPictureId() {
        return id;
    }

    public void setPictureId(Long pictureId) {
        this.id = pictureId;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }
}
