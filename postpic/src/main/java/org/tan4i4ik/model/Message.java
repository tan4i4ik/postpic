package org.tan4i4ik.model;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

    private Long id;
    private Long userSourceId;
    private Long userDestId;
    private Date datetime;
    private String text;

    public Message(Long id, Long userSourceId, Long userDestId, Date datetime, String text) {
        this.id = id;
        this.userSourceId = userSourceId;
        this.userDestId = userDestId;
        this.datetime = datetime;
        this.text = text;
    }

    public Message(Long userSourceId, Long userDestId, Date datetime, String text) {
        this.userSourceId = userSourceId;
        this.userDestId = userDestId;
        this.datetime = datetime;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserSourceId() {
        return userSourceId;
    }

    public void setUserSourceId(Long userSourceId) {
        this.userSourceId = userSourceId;
    }

    public Long getUserDestId() {
        return userDestId;
    }

    public void setUserDestId(Long userDestId) {
        this.userDestId = userDestId;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
