package org.tan4i4ik.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.tan4i4ik.serializer.UserDeserializer;
import org.tan4i4ik.serializer.UserSerializer;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@JsonSerialize(using = UserSerializer.class)
@JsonDeserialize(using = UserDeserializer.class)
public class User implements Serializable {

    private static final long serialVersionUID = 6529685098267757690L;

    private Long id;
    private String login;

    private byte[] avatar;
    private String password;
    private String name;
    private String role;
    private String email;

    private List<Picture> pictureList;

    public User(Long id, String login,  byte[] picture, String name, String email, String role, String password) {
        this.id = id;
        this.login = login;
        this.avatar = picture;
        this.password = password;
        this.role = role;
        this.name = name;
        this.email = email;
    }

    public User(String login, byte[] avatar, String password, String name, String role, String email) {
        this.login = login;
        this.avatar = avatar;
        this.password = password;
        this.name = name;
        this.role = role;
        this.email = email;
    }

    public User() {
    }

    public User(Long id, String login, byte[] accPic) {
        this.id = id;
        this.avatar = accPic;
        this.login = login;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(name, user.name) &&
                Objects.equals(role, user.role) &&
                Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, login, password, name, role, email);
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public List<Picture> getPictureList() {
        return pictureList;
    }

    public void setPictureList(List<Picture> pictureList) {
        this.pictureList = pictureList;
    }
}
