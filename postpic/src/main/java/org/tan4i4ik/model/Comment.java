package org.tan4i4ik.model;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

    private static final long serialVersionUID = 6529685098267757690L;

    private Long id;
    private Long pictureId;
    private Long userId;
    private Date datetime;
    private String text;

    public Comment(Long id, Long pictureId, Long userId, Date date, String text) {
        this.id = id;
        this.pictureId = pictureId;
        this.userId = userId;
        this.datetime = date;
        this.text = text;
    }


    public Comment() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPicture() {
        return pictureId;
    }

    public void setPicture(Long picture) {
        this.pictureId = picture;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return datetime;
    }

    public void setDate(Date date) {
        this.datetime = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
