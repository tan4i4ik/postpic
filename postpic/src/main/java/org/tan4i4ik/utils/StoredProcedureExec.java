package org.tan4i4ik.utils;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.object.StoredProcedure;

public class StoredProcedureExec extends StoredProcedure {
    public StoredProcedureExec(JdbcTemplate jdbcTemplate, String name) {
        super(jdbcTemplate, name);
        setFunction(false);
    }
}
