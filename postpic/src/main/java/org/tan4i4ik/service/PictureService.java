package org.tan4i4ik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.tan4i4ik.dao.CommentDao;
import org.tan4i4ik.dao.PictureDao;
import org.tan4i4ik.dao.UserDao;
import org.tan4i4ik.model.Picture;
import org.tan4i4ik.model.User;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PictureService {

    @Autowired
    @Qualifier("pictureDaoDB")
    private PictureDao pictureDao;

    @Autowired
    @Qualifier("userDaoDB")
    private UserDao userDao;

    @Autowired
    @Qualifier("commentDaoDB")
    private CommentDao commentDao;



    public Picture addPicture(String login, byte[] pic ) {
        User user = userDao.get(login);
        return pictureDao.add(user.getId(), pic);
    }


    public Picture addPicture(Long userId, byte[] pic ) {
        return pictureDao.add(userId, pic);
    }

    public List<Picture> getListPics() {
        List<Picture> list = pictureDao.getListPic().stream().sorted(Comparator.comparing(Picture::getDatetime).reversed()).collect(Collectors.toList());

        for (Picture picture : list) {
            picture.setCommentList(commentDao.listComment(picture.getPictureId()));
        }
        return list;

    }

    public List<Picture> getListPicByUser(String login) {
        User user = userDao.get(login);

        return pictureDao.getListPicByUser(user.getId());
    }

    public boolean deletePicture(Long userId, Long pictureId) {

        return pictureDao.deletePicture(userId, pictureId);
    }


    public Picture get(Long pictureId) {
        Picture picture = pictureDao.getPicture(pictureId);
        picture.setCommentList(commentDao.listComment(pictureId));
        return picture;
    }
}
