package org.tan4i4ik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.tan4i4ik.dao.CommentDao;
import org.tan4i4ik.dao.PictureDao;
import org.tan4i4ik.dao.UserDao;
import org.tan4i4ik.model.Picture;
import org.tan4i4ik.model.User;

import java.util.List;


@Service
public class UserService {

    @Autowired
    @Qualifier("userDaoDB")
    private UserDao userDao;

    @Autowired
    @Qualifier("pictureDaoDB")
    private PictureDao pictureDao;

    @Autowired
    @Qualifier("commentDaoDB")
    private CommentDao commentDao;

    public Long createUser(String login, String name, byte[] pic, String email, String password){
        return userDao.add(login,name, pic,email, "USER",password);
    }
    public boolean login(String login, String password) {
        User user = userDao.get(login);
        if(user == null) {
            return false;
        }
        if(user.getPassword().equals(password)) {
            return true;
        }
        return false;
    }

    public Long loginUser(String login, String password) {
       User user = userDao.get(login);
       if(user == null) {
           return -1L;
       }
       if(user.getPassword().equals(password)) {
           return user.getId();
       } else {
           return -1L;
       }
    }

    public User getUser(String login) {
        return userDao.get(login);
    }

    public User getUser(Long userId) {
        return userDao.get(userId);
    }

    public User getShortUser(Long userId) {
        User user = userDao.get(userId);
        return new User(userId,user.getLogin(),user.getAvatar());
    }

    public boolean editUser(Long userId, User user) {
        if(user.getPassword().isEmpty()) {
            User oldUser = userDao.get(userId);
            user.setPassword(oldUser.getPassword());
        }
        return userDao.edit(userId, user)==null? false: true;
    }

    public User editUser(String login, User user) {
        if(user.getPassword().isEmpty()) {
            User oldUser = userDao.get(login);
            user.setPassword(oldUser.getPassword());
        }
        return userDao.edit(login, user);
    }


    public boolean deleteUser(Long userId) {
        return userDao.delete(userId);
    }

    public boolean deleteUser(String login) {
        return userDao.delete(login);
    }



    public List<Picture> pictures(Long userId) {
        return pictureDao.getListPicByUser(userId);
    }


    public List<User> shortUserInfo() {
        return userDao.getUserShortData();
    }


    public User getFullUser(Long userId) {
        User user = userDao.get(userId);
        List<Picture> pictures = pictureDao.getListPicByUser(userId);
        for (Picture picture: pictures) {
            picture.setCommentList(commentDao.listComment(picture.getPictureId()));
        }
        user.setPictureList(pictures);

        return user;
    }
}
