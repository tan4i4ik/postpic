package org.tan4i4ik.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import org.tan4i4ik.dao.CommentDao;
import org.tan4i4ik.dao.PictureDao;
import org.tan4i4ik.dao.UserDao;
import org.tan4i4ik.model.Comment;
import org.tan4i4ik.model.Picture;
import org.tan4i4ik.model.User;


import java.util.List;

@Service
public class CommentService {

    @Autowired
    @Qualifier("commentDaoDB")
    private CommentDao commentDao;

    @Autowired
    @Qualifier("pictureDaoDB")
    private PictureDao pictureDao;

    @Autowired
    @Qualifier("userDaoDB")
    private UserDao userDao;


    public Comment addComment(Long userId, Long pictureId, String text) {
        return commentDao.addComment(pictureId,userId,text);
    }

    public List<Comment> getComments(Long pictureId) {
        return commentDao.listComment(pictureId);
    }


    public boolean deleteComment( Long commentId, Long pictureId){
        return commentDao.deleteComment(commentId,pictureId);
    }
}
