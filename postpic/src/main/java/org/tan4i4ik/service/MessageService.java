package org.tan4i4ik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.tan4i4ik.dao.MessageDao;
import org.tan4i4ik.model.Message;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageService {

    @Autowired
    @Qualifier("messageDaoDB")
    private MessageDao messageDao;

    public Message addMessage(Long idUserSource, Long idUserDest, String text) {
        return messageDao.add(idUserSource, idUserDest, text);
    }

    public boolean deleteMessage(Long userId, Long messageId){
        return messageDao.delete(userId, messageId);
    }


    public boolean editMessage(Long messageId, Long userId, String text) {
        Message message = messageDao.get(messageId);
        if(message.getUserSourceId().equals(userId)) {
            return messageDao.edit(messageId, text);
        }
        return false;
    }

    public List<Message> getChat(Long userId, Long userDestId) {
        List<Message> chat = messageDao.chat(userId, userDestId).stream().sorted(Comparator.comparing(Message::getDatetime)).collect(Collectors.toList());
        if(!chat.isEmpty()) {
            return chat;
        }
        return new ArrayList<>();
    }
}

