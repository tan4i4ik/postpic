package org.tan4i4ik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.tan4i4ik.model.Picture;
import org.tan4i4ik.model.User;
import org.tan4i4ik.service.UserService;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService us;

    @PostMapping(value = "/adduser")
    @ResponseBody
    public Long create (@RequestBody User user) {
        return us.createUser(user.getLogin(),user.getName(), user.getAvatar(), user.getEmail(), user.getPassword());
    }

    @PostMapping("/edit/user={userId}")
    @ResponseBody
    public boolean edit(@PathVariable Long userId, @RequestBody User user) {
        return us.editUser(userId, user);
    }



    @PostMapping("/getuser")
    @ResponseBody
    public User getByLogin (@RequestBody String login) {
        return us.getUser(login);
    }


    @PostMapping("/userById")
    @ResponseBody
    public User getById (@RequestBody Long userId) {
        return us.getUser(userId);
    }


    @PostMapping("/login")
    @ResponseBody
    public Long login (@RequestBody User user) {
        return us.loginUser(user.getLogin(), user.getPassword());
    }

    @PostMapping("/deleteuser")
    @ResponseBody
    public boolean delete(@RequestBody Long userId) {
        return us.deleteUser(userId);
    }




    @PostMapping("/user={userId}/pictures")
    @ResponseBody
    public List<Picture> pictures(@PathVariable Long userId) {
        return us.pictures(userId);
    }


    @GetMapping("/usersdata")
    @ResponseBody
    public List<User> shortUsers() {
        List<User> list = us.shortUserInfo();
        return list;
    }

    @GetMapping("/usershortdata/userid={userId}")
    @ResponseBody
    public User shortUser(@PathVariable Long userId) {
        User user = us.getShortUser(userId);
        return user;
    }


    @GetMapping("/getfulluser/userid={userId}")
    @ResponseBody
    public User getFullUser (@PathVariable Long userId) {
        return us.getFullUser(userId);
    }



}
