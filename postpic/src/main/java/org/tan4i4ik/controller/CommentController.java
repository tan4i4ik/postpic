package org.tan4i4ik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.tan4i4ik.model.Comment;
import org.tan4i4ik.service.CommentService;

import java.util.List;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("/user={userId}&picture={pictureId}/addcomment")
    @ResponseBody
    public Comment addComment(@PathVariable Long userId, @PathVariable Long pictureId, @RequestBody String text) {
        return commentService.addComment(userId,pictureId,text);
    }

    @DeleteMapping("/user={userId}&picture={pictureId}&comment={commentId}/delete")
    @ResponseBody
    public boolean deleteComment(@PathVariable Long userId, @PathVariable Long pictureId, @PathVariable Long commentId) {
        boolean result = commentService.deleteComment(commentId, pictureId);
        return result;
    }

    @PostMapping("/user={login}&picture={pictureId}/getcomments")
    @ResponseBody
    public List<Comment> getPictureComments(@PathVariable Long pictureId) {
        return commentService.getComments(pictureId);
    }


}

