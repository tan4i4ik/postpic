package org.tan4i4ik.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.tan4i4ik.model.Picture;
import org.tan4i4ik.service.PictureService;

import java.util.List;

@Controller
public class PictureController {

    @Autowired
    PictureService ps;


    @PostMapping("/user={userId}/addpicture")
    @ResponseBody
    public Picture add(@PathVariable Long userId, @RequestBody Picture picture) {
        return ps.addPicture(userId, picture.getPicture());
    }


    @DeleteMapping("/userid={userId}&pic={pictureid}/delete")
    @ResponseBody
    public boolean delete (@PathVariable Long userId, @PathVariable Long pictureid) {
        return ps.deletePicture(userId,pictureid);
    }

    @GetMapping("/picture={pictureid}")
    @ResponseBody
    public Picture get(@PathVariable Long pictureid) {
        return ps.get(pictureid);
    }

    @GetMapping("/pictures")
    @ResponseBody
    public List<Picture> allPictures() {
        List<Picture> list = ps.getListPics();
        return list;
    }

}
