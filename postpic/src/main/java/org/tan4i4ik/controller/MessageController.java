package org.tan4i4ik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.tan4i4ik.model.Message;
import org.tan4i4ik.service.MessageService;

import java.util.List;

@Controller
public class MessageController {

    @Autowired
    private MessageService messageService;

    @PostMapping("/userSource={idUserSource}&userDest={idUserDest}/addmessage")
    @ResponseBody
    public Message addMessage(@PathVariable Long idUserSource, @PathVariable Long idUserDest, @RequestBody String text) {
        return messageService.addMessage(idUserSource,idUserDest,text);
    }

    @DeleteMapping("/userId={userId}&messageId={messageId}/delete")
    @ResponseBody
    public boolean deleteMessage(@PathVariable Long userId, @PathVariable Long messageId) {
        return messageService.deleteMessage(userId, messageId);
    }

    @GetMapping("/userSource={idUserSource}&userDest={idUserDest}/get")
    @ResponseBody
    public List<Message> messageList(@PathVariable Long idUserSource, @PathVariable Long idUserDest) {
        return messageService.getChat(idUserSource, idUserDest);
    }
}
