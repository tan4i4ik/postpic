package org.tan4i4ik.dao.db;

import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;
import org.tan4i4ik.dao.MessageDao;
import org.tan4i4ik.model.Message;
import org.tan4i4ik.utils.StoredProcedureExec;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Repository("messageDaoDB")
public class MessageDaoImpl implements MessageDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String UPDATE = "UPDATE message_pics SET text = ? WHERE message_id = ?";
    private final String SELECT_MESSAGE = "SELECT message_id, user_id_source, user_id_dest, created_date, text FROM message_pics WHERE message_id = ?";
    private final String GET_CHAT = "SELECT m.message_id, m.user_id_source, m.user_id_dest, m.created_date, m.text FROM message_pics m INNER JOIN message_deleted_status_pics mds ON mds.message_id = m.message_id WHERE ((m.user_id_dest = ? AND  m.user_id_source = ?) OR  (m.user_id_dest = ? AND  m.user_id_source = ?)) AND (mds.msg_del_by_user <> ? OR mds.msg_del_by_user IS NULL)";
    private RowMapper<Message> mapper = (rs, rowNum) -> new Message(rs.getLong("message_id"),rs.getLong("user_id_source"), rs.getLong("user_id_dest"), rs.getTimestamp("created_date"), rs.getString("text"));


    @Override
    public Message add(Long idSourceUser, Long idDestUser, String text) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String now = LocalDateTime.now().format(formatter);
        StoredProcedure sp = new StoredProcedureExec(jdbcTemplate, "insert_message_pics");
        SqlParameter userIdSourceParam = new SqlParameter("p_source_user_id", OracleTypes.INTEGER);
        SqlParameter userIdDestParamParam = new SqlParameter("p_dest_user_id", OracleTypes.INTEGER);
        SqlParameter createdDataParam = new SqlParameter("p_created_data", OracleTypes.VARCHAR);
        SqlParameter messageParam = new SqlParameter("p_txt", OracleTypes.VARCHAR);
        SqlOutParameter resultParam = new SqlOutParameter("p_id", OracleTypes.INTEGER);
        SqlParameter[] paramArray = {userIdSourceParam,userIdDestParamParam,createdDataParam,messageParam,resultParam};
        sp.setParameters(paramArray);
        sp.compile();


        try {
            Map procResult = sp.execute(idSourceUser, idDestUser, now, text);
            Message message =  new Message(Long.valueOf((Integer)procResult.get("p_id")), idSourceUser, idDestUser,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(now),text);
            return message;
        }
        catch (DataAccessException e) {
            e.printStackTrace();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean delete(Long userId, Long messageId) {
        StoredProcedure sp = new StoredProcedureExec(jdbcTemplate, "DELETE_MESSAGE");
        SqlParameter userParam = new SqlParameter("p_usr_id", OracleTypes.INTEGER);
        SqlParameter messageParam = new SqlParameter("p_msg_id", OracleTypes.INTEGER);
        SqlParameter[] paramArray = {userParam, messageParam};
        sp.setParameters(paramArray);
        sp.compile();


        try {
            sp.execute(userId,messageId);
            return true;
        }
        catch (DataAccessException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean edit(Long messageId, String text) {
        int res = jdbcTemplate.update(UPDATE, new Object[]{text,messageId});
        return (res == 1) ? true : false;
    }

    @Override
    public List<Message> chat(Long idCurrUser, Long idUserDest) {
        return jdbcTemplate.query(GET_CHAT, new Object[]{idCurrUser, idUserDest, idUserDest, idCurrUser, idCurrUser}, mapper);
    }

    @Override
    public Message get(Long messageId) {
        List<Message> list = jdbcTemplate.query(SELECT_MESSAGE, new Object[]{messageId}, mapper);
        if(list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
}
