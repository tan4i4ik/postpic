package org.tan4i4ik.dao.db;

import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;
import org.tan4i4ik.dao.UserDao;
import org.tan4i4ik.model.User;
import org.tan4i4ik.utils.StoredProcedureExec;

import java.util.List;
import java.util.Map;


@Repository("userDaoDB")
public class UserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final String SELECT_ALL = "SELECT u.user_id, u.login, u.avatar, u.name, u.email, r.role_name, u.password FROM USER_PICS u INNER JOIN USER_ROLE_PICS r ON u.role_id = r.role_id ";
    private final String SELECT_USER_BY_ID = "SELECT u.user_id, u.login, u.avatar, u.name, u.email, r.role_name, u.password FROM USER_PICS u INNER JOIN USER_ROLE_PICS r ON u.role_id = r.role_id WHERE u.user_id = ? ";
    private final String SELECT_USER_BY_LOGIN = "SELECT u.user_id, u.login, u.avatar, u.name, u.email, r.role_name, u.password FROM USER_PICS u INNER JOIN USER_ROLE_PICS r ON u.role_id = r.role_id WHERE u.login = ? ";
    private final String DELETE_USER_BY_LOGIN = "DELETE USER_PICS WHERE login = ?";
    private final String DELETE_USER_BY_ID = "DELETE USER_PICS WHERE user_id = ?";
    private final String UPDATE_BY_LOGIN = "UPDATE USER_PICS SET name = ?, email = ?, password = ?, avatar = ? where login = ?";
    private final String UPDATE_BY_ID = "UPDATE USER_PICS SET name = ?, email = ?, password = ?, avatar = ? where user_id = ?";
    private final String SELECT_USER_SHORT = "SELECT user_id, login, avatar FROM USER_PICS";


    private RowMapper<User> mapperUser = (rs, rowNum) -> new User(rs.getLong("user_id"),rs.getString("login"), rs.getBytes("avatar"), rs.getString("name"), rs.getString("email"),rs.getString("role_name"),rs.getString("password"));
    private RowMapper<User> mapperShortUser = (rs, rowNum) -> new User(rs.getLong("user_id"),rs.getString("login"), rs.getBytes("avatar"));

    @Override
    public Long add(String login, String name, byte[] accPic, String email, String role, String password) {
        StoredProcedure sp = new StoredProcedureExec(jdbcTemplate, "INSERT_USER_PICS");

        SqlParameter loginParam = new SqlParameter("p_login", OracleTypes.VARCHAR);
        SqlParameter picParam = new SqlParameter("p_acc_pic", OracleTypes.BINARY);
        SqlParameter nameParam = new SqlParameter("p_name", OracleTypes.VARCHAR);
        SqlParameter emailParam = new SqlParameter("p_email", OracleTypes.VARCHAR);
        SqlParameter roleParam = new SqlParameter("p_role", OracleTypes.VARCHAR);
        SqlParameter passwdParam = new SqlParameter("p_password", OracleTypes.VARCHAR);
        SqlOutParameter resultParam = new SqlOutParameter("p_result", OracleTypes.INTEGER);
        SqlParameter[] paramArray = {loginParam,picParam, nameParam, emailParam,roleParam,passwdParam,resultParam};
        sp.setParameters(paramArray);
        sp.compile();

        try {
            Map procResult = sp.execute(login, accPic, name,email,role,password);
            Integer i = (Integer)procResult.get("p_result");
            return new Long(i);
        }
        catch (DuplicateKeyException e) {
            System.out.println("Login was created before");
        }
        catch (DataAccessException e) {
            e.printStackTrace();
        }
        return -1L;
    }

    public boolean isPresentUser(String login) {
        List<User> list = jdbcTemplate.query(SELECT_USER_BY_LOGIN, new Object[]{login}, mapperUser);
        if (list.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Long id) {
        int res = jdbcTemplate.update(DELETE_USER_BY_ID, new Object[]{id});
        return (res == 1) ? true : false;
    }

    @Override
    public boolean delete(String login) {
        int res = jdbcTemplate.update(DELETE_USER_BY_LOGIN, new Object[]{login});
        return (res == 1) ? true : false;
    }

    @Override
    public User edit(Long id, User user) {
        int res = jdbcTemplate.update(UPDATE_BY_ID, new Object[]{user.getName(), user.getEmail(), user.getPassword(),user.getAvatar(), id});
        return (res == 1) ? user : null;
    }

    @Override
    public User edit(String login, User user) {
        int res = jdbcTemplate.update(UPDATE_BY_LOGIN, new Object[]{user.getName(), user.getEmail(), user.getPassword(), user.getAvatar(),login});
        return (res == 1) ? user : null;
    }

    @Override
    public List<User> getUsers() {
        return jdbcTemplate.query(SELECT_ALL, mapperUser);
    }

    @Override
    public User get(Long userId) {
        List<User> list = jdbcTemplate.query(SELECT_USER_BY_ID, new Object[]{userId}, mapperUser);
        if(list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public User get(String login) {
        List<User> list = jdbcTemplate.query(SELECT_USER_BY_LOGIN, new Object[]{login}, mapperUser);
        if(list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<User> getUserShortData() {
        return jdbcTemplate.query(SELECT_USER_SHORT, mapperShortUser);
    }

}
