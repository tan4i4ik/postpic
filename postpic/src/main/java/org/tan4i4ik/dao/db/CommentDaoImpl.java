package org.tan4i4ik.dao.db;

import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;
import org.tan4i4ik.dao.CommentDao;
import org.tan4i4ik.model.Comment;
import org.tan4i4ik.utils.StoredProcedureExec;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Repository("commentDaoDB")
public class CommentDaoImpl implements CommentDao {

    private final String SELECT_COMMENTS_BY_PICTURE_ID = "SELECT comment_id, user_id, picture_id, created_date, text FROM comment_pics WHERE picture_id = ?";
    private final String DELETE = "DELETE comment_pics WHERE comment_id = ?";
    private final String SELECT_COMMENT_BY_ID = "SELECT comment_id, user_id, picture_id, created_date, text FROM comment_pics WHERE comment_id = ?";


    private RowMapper<Comment> mapperComment = (rs, rowNum) -> new Comment(rs.getLong("comment_id"),rs.getLong("picture_id"),  rs.getLong("user_id"), rs.getTimestamp("created_date"), rs.getString("text"));



    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Comment addComment(Long pictureId, Long userId, String text) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String now = LocalDateTime.now().format(formatter);
        StoredProcedure sp = new StoredProcedureExec(jdbcTemplate, "insert_comment_pics");
        SqlParameter pictureDataParam = new SqlParameter("p_picture_id", OracleTypes.INTEGER);
        SqlParameter userIdParamParam = new SqlParameter("p_user_id", OracleTypes.INTEGER);
        SqlParameter createdDataParam = new SqlParameter("p_created_data", OracleTypes.VARCHAR);
        SqlParameter textParam = new SqlParameter("p_text", OracleTypes.VARCHAR);
        SqlOutParameter resultParam = new SqlOutParameter("p_id", OracleTypes.INTEGER);
        SqlParameter[] paramArray = {pictureDataParam,userIdParamParam,createdDataParam,textParam, resultParam};
        sp.setParameters(paramArray);
        sp.compile();


        try {
            Map procResult = sp.execute(pictureId, userId, now,text);
            Comment picture =  new Comment(Long.valueOf((Integer)procResult.get("p_id")), pictureId, userId,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(now), text);

            return picture;
        }
        catch (DataAccessException e) {
            e.printStackTrace();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteComment(Long commentId, Long pictureId) {
        int res = jdbcTemplate.update(DELETE, new Object[]{commentId});
        return (res == 1) ? true: false;
    }

    @Override
    public List<Comment> listComment(Long pictureId) {
        return jdbcTemplate.query(SELECT_COMMENTS_BY_PICTURE_ID, new Object[]{pictureId}, mapperComment);
    }

    @Override
    public Comment getComment(Long userId, Long commentId) {
        return jdbcTemplate.query(SELECT_COMMENT_BY_ID, new Object[]{commentId}, mapperComment).get(0);
    }
}
