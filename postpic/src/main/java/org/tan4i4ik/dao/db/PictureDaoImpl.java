package org.tan4i4ik.dao.db;

import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;
import org.tan4i4ik.dao.PictureDao;
import org.tan4i4ik.model.Picture;
import org.tan4i4ik.utils.StoredProcedureExec;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("pictureDaoDB")
public class PictureDaoImpl implements PictureDao {

    private final String SELECT_PICTURES_BY_USER = "SELECT pic_id, user_id, picture_data, created_date FROM picture_pics WHERE user_id = ?";
    private final String SELECT_PICTURES = "SELECT pic_id, user_id, picture_data, created_date FROM picture_pics";
    private final String SELECT_PICTURE = "SELECT pic_id, user_id, picture_data, created_date FROM picture_pics WHERE pic_id = ?";
    private final String DELETE_PICTURE = "Delete From picture_pics Where pic_id = ?";

    private RowMapper<Picture> mapperPicture = (rs, rowNum) -> new Picture(rs.getLong("pic_id"),rs.getLong("user_id"),  rs.getBytes("picture_data"), rs.getTimestamp("created_date"));


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Picture add(Long userId, byte[] pic) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String now = LocalDateTime.now().format(formatter);
        StoredProcedure sp = new StoredProcedureExec(jdbcTemplate, "insert_pictures_pics");
        SqlParameter pictureDataParam = new SqlParameter("p_picture_data", OracleTypes.BINARY);
        SqlParameter userIdParamParam = new SqlParameter("p_user_id", OracleTypes.INTEGER);
        SqlParameter createdDataParam = new SqlParameter("p_created_data", OracleTypes.VARCHAR);
        SqlOutParameter resultParam = new SqlOutParameter("p_id", OracleTypes.INTEGER);
        SqlParameter[] paramArray = {pictureDataParam,userIdParamParam,createdDataParam,resultParam};
        sp.setParameters(paramArray);
        sp.compile();


        try {
            Map procResult = sp.execute(pic, userId, now);
            Picture picture =  new Picture(Long.valueOf((Integer)procResult.get("p_id")), userId,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(now));
            return picture;
        }
        catch (DataAccessException e) {
            e.printStackTrace();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deletePicture(Long userId, Long pictureId) {
        int res = jdbcTemplate.update(DELETE_PICTURE, new Object[]{pictureId});
        return (res == 1) ? true : false;

    }

    @Override
    public List<Picture> getListPic() {
        List<Picture> list = jdbcTemplate.query(SELECT_PICTURES, mapperPicture);
        return list;
    }

    @Override
    public List<Picture> getListPicByUser(Long userId) {
        return jdbcTemplate.query(SELECT_PICTURES_BY_USER, new Object[]{userId}, mapperPicture);
    }

    @Override
    public Picture getPicture(Long pictureId) {
        return jdbcTemplate.query(SELECT_PICTURE, new Object[]{pictureId}, mapperPicture).get(0);
    }
}
