package org.tan4i4ik.dao;

import org.tan4i4ik.model.Message;

import java.util.List;

public interface MessageDao {
    Message add(Long idSourceUser, Long idDestUser, String text);
    boolean delete(Long userId, Long messageId);
    boolean edit(Long messageId, String text);
    List<Message> chat(Long idCurrUser, Long idUserDest);
    Message get(Long messageId);
}
