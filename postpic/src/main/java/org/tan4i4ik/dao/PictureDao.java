package org.tan4i4ik.dao;

import org.tan4i4ik.model.Picture;

import java.util.List;

public interface PictureDao {

    Picture add(Long userId, byte[] pic);
    boolean deletePicture(Long userId,Long pictureId);
    List<Picture> getListPic();
    List<Picture> getListPicByUser(Long userId);
    Picture getPicture(Long pictureId);
}
