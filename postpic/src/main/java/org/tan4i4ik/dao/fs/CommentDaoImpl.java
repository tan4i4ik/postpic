package org.tan4i4ik.dao.fs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.tan4i4ik.dao.CommentDao;
import org.tan4i4ik.dao.PictureDao;
import org.tan4i4ik.model.Comment;
import org.tan4i4ik.model.Picture;

import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Repository("commentDaoFS")
public class CommentDaoImpl implements CommentDao {

    @Autowired
    @Qualifier("pictureDaoFS")
    private PictureDao pd;


    @Override
    public Comment addComment(Long pictureId, Long userId, String text) {
        Picture picture = pd.getPicture(pictureId);
        File dir = new File("data/users/" + picture.getUserId() + "/pics/" +pictureId + "/comments");
        LocalDateTime ldt = LocalDateTime.now();
        Long id = ldt.atZone(ZoneId.of("America/New_York")).toEpochSecond();
        if(!dir.exists()) {
            dir.mkdir();
        }
        try {
            File f =  new File("data/users/" + userId + "/pics/" + pictureId + "/comments/" + id + ".txt");

            /**********create file of user information***************/
            FileOutputStream fos = new FileOutputStream(f);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(new Comment(id, pictureId, userId, Date.from(ldt.atZone(ZoneId.of("America/New_York")).toInstant()), text));

            oos.close();
            fos.close();

            return new Comment(id, pictureId, userId, Date.from(ldt.atZone(ZoneId.of("America/New_York")).toInstant()), text);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteComment(Long commentId, Long pictureId) {
        File file = new File("data/users/" + pictureId + "/pics/" + pictureId + "/comments/" + commentId +".txt");
        return file.delete();
    }

    @Override
    public List<Comment> listComment(Long pictureId) {
        List<Comment> list = new ArrayList<>();
        Picture picture = pd.getPicture(pictureId);
        File dir = new File("data/users/" + picture.getUserId() + "/pics/" + pictureId + "/comments");
        if(!dir.exists()) {
            return null;
        }

        for(File file: dir.listFiles()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fis);

                Comment comment = (Comment) ois.readObject();
                list.add(comment);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }


        return list;
    }

    @Override
    public Comment getComment(Long userId, Long commentId) {
        return null;
    }

}
