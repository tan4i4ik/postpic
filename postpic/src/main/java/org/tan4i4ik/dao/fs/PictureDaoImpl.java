package org.tan4i4ik.dao.fs;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Repository;
import org.tan4i4ik.dao.PictureDao;
import org.tan4i4ik.dao.UserDao;
import org.tan4i4ik.model.Picture;
import org.tan4i4ik.model.User;

import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository("pictureDaoFS")
public class PictureDaoImpl implements PictureDao {

    @Override
    public Picture add(Long userId, byte[] pic) {
        LocalDateTime ldt = LocalDateTime.now();
        Long id = ldt.atZone(ZoneId.of("America/New_York")).toEpochSecond();


        File directoryPics = new File("data/users/" + userId + "/pics/");
        if (!directoryPics.exists()) {
          directoryPics.mkdir();
        }
        File pictureDir = new File("data/users/" + userId + "/pics/" +id);
        if (!pictureDir.exists()) {
           pictureDir.mkdir();
        }


        Picture picture = new Picture(id, userId, pic ,Date.from(ldt.atZone(ZoneId.of("America/New_York")).toInstant()));
        try {
            FileOutputStream fos = new FileOutputStream("data/users/" + userId + "/pics/" +id + "/" + id +".txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(picture);
            return picture;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public boolean deletePicture(Long userId,Long pictureId) {
        File dir = new File("data/users/" + userId + "/pics/" + pictureId);
        File image = new File("data/pictures/" + pictureId + ".jpeg");
        boolean flag = false;
        try {
            FileUtils.deleteDirectory(dir);
            flag = image.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public List<Picture> getListPicByUser(Long userId) {
        List<Picture> list = new ArrayList<>();
        File dir = new File("data/users/" + userId + "/pics");
        if(dir.exists()) {
            for (String picDir : dir.list())
                try {
                    FileInputStream file = new FileInputStream("data/users/" + userId + "/pics/" + picDir + "/" + picDir + ".txt");

                    ObjectInputStream ois = new ObjectInputStream(file);
                    list.add((Picture) ois.readObject());

                } catch (EOFException | FileNotFoundException e) {

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
        }

        return list;
    }

    @Override
    public Picture getPicture(Long pictureId) {
        File usersDir  = new File("data/users");
        String[] dir = usersDir.list();
        File picFile = null;

        for(String userDir: dir) {
            for(String pic: new File("data/users/" + userDir + "/pics/").list()) {
                if(pic.equals(pictureId.toString())) {
                    picFile = new File("data/users/" + userDir + "/pics/" + pic + "/" + pic + ".txt");
                }
            }
        }
        try {
            FileInputStream fis = new FileInputStream(picFile);

            ObjectInputStream ois = new ObjectInputStream(fis);
            return (Picture) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Picture> getListPic() {
        List<Picture> list = new ArrayList<>();
        File dir = new File("data/users/");
        for(String userDir: dir.list()) {
            File picsDir = new File("data/users/" + userDir +"/pics");
            if(picsDir.exists()) {
                for (String picId : picsDir.list()) {
                        try {
                            FileInputStream file = new FileInputStream("data/users/" + userDir + "/pics/" +picId + "/" + picId +".txt");

                            ObjectInputStream ois = new ObjectInputStream(file);
                            list.add((Picture) ois.readObject());

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

        return list;
    }

}
