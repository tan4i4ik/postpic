package org.tan4i4ik.dao.fs;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Repository;
import org.tan4i4ik.dao.UserDao;
import org.tan4i4ik.model.User;

import javax.jws.soap.SOAPBinding;
import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Repository("userDaoFS")
public class UserDaoImpl implements UserDao {

    public UserDaoImpl() {
        if(!new File("data").exists()) {
            new File("data").mkdir();
        }

        if(!new File("data/users").exists()) {
            new File("data/users").mkdir();
        }


    }

    @Override
    public Long add(String login, String name, byte[] accPic, String email, String role, String password) {
        Long id = LocalDateTime.now().atZone(ZoneId.of("America/New_York")).toEpochSecond();
        User u = new User(id, login, null, name, email,"USER", password);

        if(!isPresentUser(login)) {
            File directory = new File("data/users/" + id );
            if(!directory.exists()) {
                directory.mkdir();
            }

            try {
                File f =  new File("data/users/" + id + "/" + id + ".txt");

                /**********create file of user information***************/
                FileOutputStream fos = new FileOutputStream(f);
                ObjectOutputStream oos = new ObjectOutputStream(fos);


                oos.writeObject(u);

                oos.close();
                fos.close();

                return id;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return -1L;
    }

    @Override
    public boolean isPresentUser(String login) {
        File folder = new File("data/users");
        if (folder.exists()) {
            for(String file: folder.list()) {
                if (file.equals(login)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean delete(Long id) {
        File file = new File("data/users/" + id);
        try {
            FileUtils.deleteDirectory(file);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public boolean delete(String login) {
        User user = get(login);
        return delete(user.getId());
    }



    @Override
    public User edit(Long id, User user) {
        File f = new File("data/users/" + id +"/" + id + ".txt");
        if(f.exists()) {
            try {
                FileOutputStream fos = new FileOutputStream(f);
                ObjectOutputStream oos = new ObjectOutputStream(fos);

                oos.writeObject(user);

                oos.close();
                fos.close();
                return user;

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        return null;
    }

    @Override
    public User edit(String login, User user) {
        User getUser = get(login);
        return edit(user.getId(), user);
    }


    @Override
    public List<User> getUsers() {
        File directory = new File("data/users");
        List<User> list = new ArrayList<>();
        for(String dirName: directory.list()) {
            try {
                FileInputStream file = new FileInputStream("data/users/" + dirName + "/" + dirName + ".txt");
                ObjectInputStream inputStream = new ObjectInputStream(file);
                list.add((User) inputStream.readObject());


            } catch (FileNotFoundException e) {
                System.out.println("User wasn't created. Please create user before login");
                return null;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            }

        }
        return list;
    }

    @Override
    public User get(Long userId) {
        User user = null;

        try {
            FileInputStream file = new FileInputStream("data/users/" + userId + "/" + userId + ".txt");
            ObjectInputStream inputStream = new ObjectInputStream(file);
            return  (User) inputStream.readObject();


        } catch (FileNotFoundException e) {
            System.out.println("User wasn't created. Please create user before login");
            return null;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User get(String login) {
        User user;
        File dirList =new File("data/users/");
        for(String userId: dirList.list()) {
            try {
                FileInputStream file = new FileInputStream("data/users/" + userId + "/" + userId + ".txt");
                ObjectInputStream inputStream = new ObjectInputStream(file);

                user = (User) inputStream.readObject();
                if (user.getLogin().equals(login)) {
                    return user;
                }

            } catch (FileNotFoundException e) {
                System.out.println("User wasn't created. Please create user before login");
                user = null;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }



        return null;
    }



    @Override
    public List<User> getUserShortData() {
        return null;
    }

}
