package org.tan4i4ik.dao.fs;

import org.springframework.stereotype.Repository;
import org.tan4i4ik.dao.MessageDao;
import org.tan4i4ik.model.Message;

import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository("messageDaoFS")
public class MessageDaoImpl implements MessageDao {

    public MessageDaoImpl () {
        if(!new File("data/messages").exists()) {
            new File("data/messages").mkdir();
        }
        if(!new File("data/messages/msg.txt").exists()) {
            try {
                new File("data/messages/msg.txt").createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    @Override
    public Message add(Long idSourceUser, Long idDestUser, String text) {
        LocalDateTime ldt = LocalDateTime.now();
        Long id = ldt.atZone(ZoneId.of("America/New_York")).toEpochSecond();
       String f;
       if(new File("data/messages/" + idDestUser + "_" + idSourceUser).exists()) {
           f = "data/messages/" + idDestUser + "_" + idSourceUser;
       } else if (new File("data/messages/" + idSourceUser + "_" + idDestUser).exists()){
           f = "data/messages/" + idSourceUser + "_" + idDestUser;
       } else {
           new File("data/messages/" + idDestUser + "_" + idSourceUser).mkdir();
           f = "data/messages/" + idDestUser + "_" + idSourceUser;
       }
        try {

            /**********create file of user information***************/
            FileOutputStream fos = new FileOutputStream(f + "/" + id);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            Message message = new Message(id,idSourceUser, idDestUser, Date.from(ldt.atZone(ZoneId.of("America/New_York")).toInstant()), text);
            oos.writeObject(message);

            oos.close();
            fos.close();

            FileWriter fw = new FileWriter(new File("data/messages/msg.txt"), true);
            fw.write(id +","+ f+"/" + id+"\n");
            fw.close();
            return message;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;


    }

    @Override
    public boolean delete(Long userId, Long messageId) {
        String path;
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader fr = new BufferedReader(new FileReader(new File("data/messages/msg.txt")));
            String line;
            while((line = fr.readLine()) != null) {
                if(line.split(",")[0].equals(String.valueOf(messageId))) {
                    new File(line.split(",")[1]).delete();
                }

                sb.append(line + "\n");
            }
            FileWriter fw = new FileWriter(new File("data/messages/msg.txt"));
            fw.write(sb.toString());
            fw.close();
            return true;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean edit(Long messageId, String text) {
        File file = null;
        String path;
        StringBuilder sb = new StringBuilder();
        Message message = get(messageId);
        message.setText(text);
        try {
            BufferedReader fr = new BufferedReader(new FileReader(new File("data/messages/msg.txt")));
            String line;
            while((line = fr.readLine()) != null) {
                if(line.split(",")[0].equals(String.valueOf(messageId))) {
                    file = new File(line.split(",")[1]);
                }

                sb.append(line + "\n");
            }
            try {

                /**********create file of user information***************/
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);


                oos.writeObject(message);

                oos.close();
                fos.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;

    }

    @Override
    public List<Message> chat(Long idCurrUser, Long idUserDest) {
        String f;
        List<Message> list = new ArrayList<>();
        if(new File("data/messages/" + idCurrUser + "_" + idUserDest).exists()) {
            f = "data/messages/" + idCurrUser + "_" + idUserDest;
        } else if (new File("data/messages/" + idUserDest + "_" + idCurrUser).exists()){
            f = "data/messages/" + idUserDest + "_" + idCurrUser;
        } else {
            return new ArrayList<>();
        }

        for(File file: new File(f).listFiles()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fis);
                list.add((Message) ois.readObject());

                ois.close();
                fis.close();

            } catch (EOFException | FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return list;
    }

    @Override
    public Message get(Long messageId) {
        File file = null;
        try {
            BufferedReader fr = new BufferedReader(new FileReader(new File("data/messages/msg.txt")));
            String line;
            while((line = fr.readLine()) != null) {
                if(line.split(",")[0].equals(String.valueOf(messageId))) {
                    file = new File(line.split(",")[1]);
                    break;
                }

            }

            try {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fis);
                Message message = ((Message) ois.readObject());
                ois.close();
                fis.close();

                return message;

            } catch (EOFException | FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }
}
