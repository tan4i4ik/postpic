package org.tan4i4ik.dao;

import org.tan4i4ik.model.User;
import java.util.List;

public interface UserDao {

        Long add(String login, String name, byte[] accPic, String email, String role, String password);
        boolean isPresentUser(String login);
        boolean delete(Long id);
        boolean delete(String login);
        User edit(Long id,User user);
        User edit(String login,User user);
        List<User> getUsers();
        User get(Long id);
        User get(String login);
        List<User> getUserShortData();

}
