package org.tan4i4ik.dao;

import org.tan4i4ik.model.Comment;
import org.tan4i4ik.model.Picture;

import java.util.List;

public interface CommentDao {
    Comment addComment(Long pictureId, Long userId, String text);
    boolean deleteComment( Long commentId, Long pictureId);
    List<Comment> listComment(Long pictureId);
    Comment getComment(Long userId, Long commentId);
}
