-----create roles table
CREATE TABLE user_role_pics(
    role_id INTEGER PRIMARY KEY,
    role_name VARCHAR(20)
);

INSERT INTO user_role_pics VALUES (1,'ADMIN');
INSERT INTO user_role_pics VALUES (2,'USER');


-----Create table of users-----
DROP TABLE user_pics;
CREATE TABLE user_pics(
    user_id INTEGER PRIMARY KEY,
    login VARCHAR2(40) UNIQUE,
    password VARCHAR2(50),
    avatar BLOB,
    name VARCHAR(50),
    role_id INTEGER,
    email VARCHAR(50),
    CONSTRAINT fk_user_role
    FOREIGN KEY(role_id)
    REFERENCES USER_ROLE_PICS(role_id)
    ON DELETE CASCADE
);


----add sequence to primary key
CREATE SEQUENCE user_seq1;

CREATE OR REPLACE TRIGGER user_pics_insert
  BEFORE INSERT ON user_pics
  FOR EACH ROW
BEGIN
  SELECT user_seq1.nextval
  INTO :new.user_id
  FROM dual;
END;

----add user
create or replace PROCEDURE insert_user_pics (p_login IN VARCHAR, p_name IN VARCHAR, p_email IN VARCHAR, p_role IN VARCHAR, p_password IN VARCHAR2, p_result OUT INTEGER) 
IS
    v_roleid user_role_pics.role_id%TYPE := 0;
BEGIN
    SELECT role_id 
    INTO v_roleid 
    FROM user_role_pics 
    WHERE role_name = p_role;
    
    IF v_roleid != 0 THEN
        INSERT 
        INTO USER_PICS(login, name, email, role_id, password) 
        VALUES (p_login, p_name, p_email, v_roleid, p_password) returning user_id into p_id;
        
    
    ELSE 
        p_result := -1;
    END IF;  
END;

---create table of pictures
DROP TABLE picture_pics;
CREATE TABLE picture_pics(
    pic_id INTEGER PRIMARY KEY,
    created_date DATE,
    user_id INTEGER,
    picture_data BLOB,
    CONSTRAINT fk_picture_user
    FOREIGN KEY(user_id)
    REFERENCES USER_PICS(user_id)
    ON DELETE CASCADE
);

----add primary key on insert
CREATE SEQUENCE picture_seq;

CREATE OR REPLACE TRIGGER picture_pics_insert
  BEFORE INSERT ON picture_pics
  FOR EACH ROW
BEGIN
  SELECT picture_seq.nextval
  INTO :new.pic_id
  FROM dual;
END;

----delete picture 
create or replace PROCEDURE delete_picture_pics (p_picture_id IN NUMBER, p_user_id IN NUMBER, p_result OUT NUMBER) 
IS
BEGIN
     Delete From picture_pics Where user_id = p_user_id And pic_id = p_picture_id;
    IF SQL%ROWCOUNT = 0  THEN 
        p_result := 0;
    ELSE
        p_result :=1;
    END IF;    
END;

---add picture
create or replace PROCEDURE insert_pictures_pics (p_picture_data IN BLOB, p_user_id IN NUMBER, p_created_date IN VARCHAR2, p_id OUT NUMBER) 
IS
BEGIN
     INSERT INTO picture_pics(picture_data, user_id, created_date) VALUES ( p_picture_data,p_user_id, TO_DATE(p_created_date, 'yyyy/mm/dd hh24:mi:ss')) returning pic_id into p_id;
END;

SELECT p.pic_id, p.user_id, p.picture_data, p.created_date, u.login, u.acc_pic FROM picture_pics p INNER JOIN user_pics u ON p.user_id = u.user_id;

SELECT c.comment_id, c.user_id as comment_user_id, u.login as comment_user_login, u.acc_pic as comment_user_avatar, p.pic_id, p.created_date, p.picture_data, p.user_id as pic_user_id,  pu.login as pic_user_login, pu.acc_pic as pic_user_avatar, c.created_date, c.text 
FROM comment_pics c 
INNER JOIN picture_pics p 
ON c.picture_id = p.pic_id 
INNER JOIN user_pics u 
ON c.user_id = u.user_id 
INNER JOIN user_pics pu ON pu.user_id = p.user_id WHERE c.comment_id = 23;

---create table of comments 
DROP TABLE comment_pics;
CREATE TABLE comment_pics(
    comment_id INTEGER PRIMARY KEY,
    user_id INTEGER,
    picture_id INTEGER,
    created_date DATE,
    text VARCHAR2(50),
    CONSTRAINT fk_comment_user
    FOREIGN KEY(user_id)
    REFERENCES USER_PICS(user_id),
    CONSTRAINT fk_comment_picture
    FOREIGN KEY(picture_id)
    REFERENCES PICTURE_PICS(pic_id)
    ON DELETE CASCADE
);

----add primary key on insert
CREATE SEQUENCE comment_seq;

CREATE OR REPLACE TRIGGER comment_pics_insert
  BEFORE INSERT ON comment_pics
  FOR EACH ROW
BEGIN
  SELECT comment_seq.nextval
  INTO :new.comment_id
  FROM dual;
END;

---add comment
create or replace PROCEDURE insert_comment_pics (p_picture_id IN NUMBER, p_user_id IN NUMBER, p_created_date IN VARCHAR2, p_txt IN VARCHAR2, p_id OUT NUMBER) 
IS
BEGIN
    INSERT INTO comment_pics(picture_id, user_id, created_date, text) VALUES ( p_picture_id,p_user_id, TO_DATE(p_created_date, 'yyyy/mm/dd hh24:mi:ss'), p_txt) returning comment_id into p_id;
END;


----create table of messages
DROP TABLE message_pics;
CREATE TABLE message_pics(    
    message_id INTEGER PRIMARY KEY,
    user_id_source INTEGER,
    user_id_dest INTEGER,
    created_date DATE,
    text VARCHAR2(500),
    CONSTRAINT fk_comment_user_dest
    FOREIGN KEY(user_id_dest)
    REFERENCES USER_PICS(user_id),
    CONSTRAINT fk_comment_user_source
    FOREIGN KEY(user_id_source)
    REFERENCES USER_PICS(user_id)
    ON DELETE CASCADE
);

---add message
create or replace PROCEDURE insert_message_pics (p_source_user_id IN NUMBER, p_dest_user_id IN NUMBER,p_created_date IN VARCHAR,  p_txt IN VARCHAR2, p_id OUT NUMBER) 
IS
BEGIN
    INSERT INTO message_pics(user_id_source, user_id_dest, created_date, text) VALUES ( p_source_user_id,p_dest_user_id, TO_DATE(p_created_date, 'yyyy/mm/dd hh24:mi:ss'), p_txt) returning message_id into p_id;
    INSERT INTO message_deleted_status_pics(message_id) VALUES (p_id);
END;


----add primary key on insert
CREATE SEQUENCE message_seq;


----create table for keeping information about deleting messages
DROP TABLE message_deleted_status_pics;
CREATE TABLE message_deleted_status_pics(    
    message_id INTEGER PRIMARY KEY,
    msg_del_by_user INTEGER,
    CONSTRAINT fk_message
    FOREIGN KEY(message_id)
    REFERENCES MESSAGE_PICS(message_id)
    ON DELETE CASCADE
);

----add triggers on create
create or replace TRIGGER message_pics_after_insert
  AFTER INSERT ON message_pics
  FOR EACH ROW
BEGIN
  INSERT INTO message_deleted_status_pics(message_id) VALUES (:new.message_id);
END;

create or replace TRIGGER message_pics_before_insert
  BEFORE INSERT ON message_pics
FOR EACH ROW
BEGIN
  SELECT message_seq.nextval
  INTO :new.message_id
  FROM dual;
END;


---delete message
create or replace PROCEDURE delete_message(p_usr_id IN INTEGER, p_msg_id IN INTEGER)
IS
    v_status_delete VARCHAR(50);
    v_usr_dest_id INTEGER;
    v_usr_source_id INTEGER;
    
BEGIN
    SELECT t.user_id_source, t.user_id_dest 
    INTO v_usr_source_id, v_usr_dest_id 
    FROM message_pics t 
    WHERE t.message_id = p_msg_id;
    
    IF v_usr_source_id = v_usr_dest_id
    THEN 
        DELETE FROM message_deleted_status_pics 
        WHERE message_id = p_msg_id;
        
        DELETE FROM message_pics 
        WHERE message_id = p_msg_id;
        RETURN;
    END IF;
    
    SELECT msg_del_by_user 
    INTO v_status_delete 
    FROM message_deleted_status_pics t 
    WHERE t.message_id = p_msg_id;
    
    IF v_status_delete IS NULL
    THEN 
        UPDATE msg_del_by_user SET msg_del_by_user = p_usr_id 
        WHERE message_id = p_msg_id;
    ELSE 
        DELETE FROM message_deleted_status_pics 
        WHERE message_id = p_msg_id;
        
        DELETE FROM message_pics 
        WHERE message_id = p_msg_id;
        
    END IF;

END;


/*********get chat************************/
SELECT m.message_id, m.user_id_source, m.user_id_dest, m.created_date, m.text, mds.msg_del_by_user FROM message_pics m INNER JOIN message_deleted_status_pics mds ON mds.message_id = m.message_id WHERE ((m.user_id_dest = 8 AND  m.user_id_source = 1) OR  (m.user_id_dest = 1 AND  m.user_id_source = 8)) AND (mds.msg_del_by_user <> 1 OR mds.msg_del_by_user IS NULL);

