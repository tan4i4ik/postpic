package cucumber.org.tan4i4ik.service;

import cucumber.api.java8.En;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.tan4i4ik.model.User;
import org.tan4i4ik.service.UserService;
import org.tan4i4ik.springboot.Application;

import static org.junit.Assert.assertEquals;


@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class LoginStepsDef implements En {

    private User user;

    @Autowired
    private UserService userService;

    public LoginStepsDef() {

        Given("^User trying to login$",() -> {
            System.out.println("User're trying to login");});
        When("^login is '(.*)' and password is '(.*)'$", (log, pass)-> {
            user = new User(log.toString(), pass.toString());
        });

        Then("^user logged in result is (false|true)$",(result) -> {
            Boolean res = userService.login(user.getLogin(), user.getPassword());
            assertEquals(res, new Boolean(result.toString()));
        });

    }

}

