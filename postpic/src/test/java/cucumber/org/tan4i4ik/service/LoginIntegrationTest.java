package cucumber.org.tan4i4ik.service;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.tan4i4ik.springboot.Application;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources"},  plugin = {"pretty", "html:target/cucumber"})
public class LoginIntegrationTest {

}
