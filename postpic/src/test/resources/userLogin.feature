Feature: Login User

Scenario Outline: Login Functionality
    Given User trying to login
    When login is '<login>' and password is '<password>'
    Then user logged in result is <result>

    Examples:
        | login | password | result |
        | kate  | Kate11   | true   |
        | kate  | Kate13   | false  |
